﻿namespace Armoury_url_maker
{
    partial class CharacterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CharacterInfoLabel = new System.Windows.Forms.Label();
            this.helmPictureBox = new System.Windows.Forms.PictureBox();
            this.neckPictureBox = new System.Windows.Forms.PictureBox();
            this.shouldersPictureBox = new System.Windows.Forms.PictureBox();
            this.cloakPictureBox = new System.Windows.Forms.PictureBox();
            this.chestPictureBox = new System.Windows.Forms.PictureBox();
            this.bracersPictureBox = new System.Windows.Forms.PictureBox();
            this.tabardPictureBox = new System.Windows.Forms.PictureBox();
            this.shirtPictureBox = new System.Windows.Forms.PictureBox();
            this.trinket2PictureBox = new System.Windows.Forms.PictureBox();
            this.trinket1PictureBox = new System.Windows.Forms.PictureBox();
            this.ring2PictureBox = new System.Windows.Forms.PictureBox();
            this.ring1PictureBox = new System.Windows.Forms.PictureBox();
            this.bootsPictureBox = new System.Windows.Forms.PictureBox();
            this.legsPictureBox = new System.Windows.Forms.PictureBox();
            this.beltPictureBox = new System.Windows.Forms.PictureBox();
            this.glovesPictureBox = new System.Windows.Forms.PictureBox();
            this.mainHandPictureBox = new System.Windows.Forms.PictureBox();
            this.offHandPictureBox = new System.Windows.Forms.PictureBox();
            this.rangedPictureBox = new System.Windows.Forms.PictureBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.titleLabel = new System.Windows.Forms.Label();
            this.classRaceLabel = new System.Windows.Forms.Label();
            this.guildLabel = new System.Windows.Forms.Label();
            this.itemInfoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.helmPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.neckPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shouldersPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cloakPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chestPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bracersPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabardPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shirtPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trinket2PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trinket1PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ring2PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ring1PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bootsPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.legsPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beltPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.glovesPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainHandPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offHandPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangedPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // CharacterInfoLabel
            // 
            this.CharacterInfoLabel.AutoSize = true;
            this.CharacterInfoLabel.Location = new System.Drawing.Point(74, 293);
            this.CharacterInfoLabel.Name = "CharacterInfoLabel";
            this.CharacterInfoLabel.Size = new System.Drawing.Size(35, 13);
            this.CharacterInfoLabel.TabIndex = 0;
            this.CharacterInfoLabel.Text = "label1";
            // 
            // helmPictureBox
            // 
            this.helmPictureBox.Location = new System.Drawing.Point(12, 45);
            this.helmPictureBox.Name = "helmPictureBox";
            this.helmPictureBox.Size = new System.Drawing.Size(56, 56);
            this.helmPictureBox.TabIndex = 1;
            this.helmPictureBox.TabStop = false;
            this.helmPictureBox.Click += new System.EventHandler(this.helmPictureBox_Click);
            // 
            // neckPictureBox
            // 
            this.neckPictureBox.Location = new System.Drawing.Point(12, 107);
            this.neckPictureBox.Name = "neckPictureBox";
            this.neckPictureBox.Size = new System.Drawing.Size(56, 56);
            this.neckPictureBox.TabIndex = 2;
            this.neckPictureBox.TabStop = false;
            this.neckPictureBox.Click += new System.EventHandler(this.neckPictureBox_Click);
            // 
            // shouldersPictureBox
            // 
            this.shouldersPictureBox.Location = new System.Drawing.Point(12, 169);
            this.shouldersPictureBox.Name = "shouldersPictureBox";
            this.shouldersPictureBox.Size = new System.Drawing.Size(56, 56);
            this.shouldersPictureBox.TabIndex = 3;
            this.shouldersPictureBox.TabStop = false;
            // 
            // cloakPictureBox
            // 
            this.cloakPictureBox.Location = new System.Drawing.Point(12, 231);
            this.cloakPictureBox.Name = "cloakPictureBox";
            this.cloakPictureBox.Size = new System.Drawing.Size(56, 56);
            this.cloakPictureBox.TabIndex = 4;
            this.cloakPictureBox.TabStop = false;
            // 
            // chestPictureBox
            // 
            this.chestPictureBox.Location = new System.Drawing.Point(12, 293);
            this.chestPictureBox.Name = "chestPictureBox";
            this.chestPictureBox.Size = new System.Drawing.Size(56, 56);
            this.chestPictureBox.TabIndex = 5;
            this.chestPictureBox.TabStop = false;
            // 
            // bracersPictureBox
            // 
            this.bracersPictureBox.Location = new System.Drawing.Point(12, 479);
            this.bracersPictureBox.Name = "bracersPictureBox";
            this.bracersPictureBox.Size = new System.Drawing.Size(56, 56);
            this.bracersPictureBox.TabIndex = 8;
            this.bracersPictureBox.TabStop = false;
            // 
            // tabardPictureBox
            // 
            this.tabardPictureBox.Location = new System.Drawing.Point(12, 417);
            this.tabardPictureBox.Name = "tabardPictureBox";
            this.tabardPictureBox.Size = new System.Drawing.Size(56, 56);
            this.tabardPictureBox.TabIndex = 7;
            this.tabardPictureBox.TabStop = false;
            // 
            // shirtPictureBox
            // 
            this.shirtPictureBox.Location = new System.Drawing.Point(12, 355);
            this.shirtPictureBox.Name = "shirtPictureBox";
            this.shirtPictureBox.Size = new System.Drawing.Size(56, 56);
            this.shirtPictureBox.TabIndex = 6;
            this.shirtPictureBox.TabStop = false;
            // 
            // trinket2PictureBox
            // 
            this.trinket2PictureBox.Location = new System.Drawing.Point(269, 479);
            this.trinket2PictureBox.Name = "trinket2PictureBox";
            this.trinket2PictureBox.Size = new System.Drawing.Size(56, 56);
            this.trinket2PictureBox.TabIndex = 17;
            this.trinket2PictureBox.TabStop = false;
            // 
            // trinket1PictureBox
            // 
            this.trinket1PictureBox.Location = new System.Drawing.Point(269, 417);
            this.trinket1PictureBox.Name = "trinket1PictureBox";
            this.trinket1PictureBox.Size = new System.Drawing.Size(56, 56);
            this.trinket1PictureBox.TabIndex = 16;
            this.trinket1PictureBox.TabStop = false;
            // 
            // ring2PictureBox
            // 
            this.ring2PictureBox.Location = new System.Drawing.Point(269, 355);
            this.ring2PictureBox.Name = "ring2PictureBox";
            this.ring2PictureBox.Size = new System.Drawing.Size(56, 56);
            this.ring2PictureBox.TabIndex = 15;
            this.ring2PictureBox.TabStop = false;
            // 
            // ring1PictureBox
            // 
            this.ring1PictureBox.Location = new System.Drawing.Point(269, 293);
            this.ring1PictureBox.Name = "ring1PictureBox";
            this.ring1PictureBox.Size = new System.Drawing.Size(56, 56);
            this.ring1PictureBox.TabIndex = 14;
            this.ring1PictureBox.TabStop = false;
            // 
            // bootsPictureBox
            // 
            this.bootsPictureBox.Location = new System.Drawing.Point(269, 231);
            this.bootsPictureBox.Name = "bootsPictureBox";
            this.bootsPictureBox.Size = new System.Drawing.Size(56, 56);
            this.bootsPictureBox.TabIndex = 13;
            this.bootsPictureBox.TabStop = false;
            // 
            // legsPictureBox
            // 
            this.legsPictureBox.Location = new System.Drawing.Point(269, 169);
            this.legsPictureBox.Name = "legsPictureBox";
            this.legsPictureBox.Size = new System.Drawing.Size(56, 56);
            this.legsPictureBox.TabIndex = 12;
            this.legsPictureBox.TabStop = false;
            // 
            // beltPictureBox
            // 
            this.beltPictureBox.Location = new System.Drawing.Point(269, 107);
            this.beltPictureBox.Name = "beltPictureBox";
            this.beltPictureBox.Size = new System.Drawing.Size(56, 56);
            this.beltPictureBox.TabIndex = 11;
            this.beltPictureBox.TabStop = false;
            // 
            // glovesPictureBox
            // 
            this.glovesPictureBox.Location = new System.Drawing.Point(269, 45);
            this.glovesPictureBox.Name = "glovesPictureBox";
            this.glovesPictureBox.Size = new System.Drawing.Size(56, 56);
            this.glovesPictureBox.TabIndex = 10;
            this.glovesPictureBox.TabStop = false;
            // 
            // mainHandPictureBox
            // 
            this.mainHandPictureBox.Location = new System.Drawing.Point(76, 479);
            this.mainHandPictureBox.Name = "mainHandPictureBox";
            this.mainHandPictureBox.Size = new System.Drawing.Size(56, 56);
            this.mainHandPictureBox.TabIndex = 19;
            this.mainHandPictureBox.TabStop = false;
            // 
            // offHandPictureBox
            // 
            this.offHandPictureBox.Location = new System.Drawing.Point(141, 479);
            this.offHandPictureBox.Name = "offHandPictureBox";
            this.offHandPictureBox.Size = new System.Drawing.Size(56, 56);
            this.offHandPictureBox.TabIndex = 20;
            this.offHandPictureBox.TabStop = false;
            // 
            // rangedPictureBox
            // 
            this.rangedPictureBox.Location = new System.Drawing.Point(207, 479);
            this.rangedPictureBox.Name = "rangedPictureBox";
            this.rangedPictureBox.Size = new System.Drawing.Size(56, 56);
            this.rangedPictureBox.TabIndex = 21;
            this.rangedPictureBox.TabStop = false;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(8, 9);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(51, 20);
            this.nameLabel.TabIndex = 22;
            this.nameLabel.Text = "Name";
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(287, 9);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(38, 20);
            this.titleLabel.TabIndex = 23;
            this.titleLabel.Text = "Title";
            // 
            // classRaceLabel
            // 
            this.classRaceLabel.AutoSize = true;
            this.classRaceLabel.Location = new System.Drawing.Point(219, 29);
            this.classRaceLabel.Name = "classRaceLabel";
            this.classRaceLabel.Size = new System.Drawing.Size(106, 13);
            this.classRaceLabel.TabIndex = 24;
            this.classRaceLabel.Text = "level race spec class";
            // 
            // guildLabel
            // 
            this.guildLabel.AutoSize = true;
            this.guildLabel.Location = new System.Drawing.Point(9, 29);
            this.guildLabel.Name = "guildLabel";
            this.guildLabel.Size = new System.Drawing.Size(43, 13);
            this.guildLabel.TabIndex = 25;
            this.guildLabel.Text = "<Guild>";
            // 
            // itemInfoLabel
            // 
            this.itemInfoLabel.AutoSize = true;
            this.itemInfoLabel.Location = new System.Drawing.Point(74, 62);
            this.itemInfoLabel.Name = "itemInfoLabel";
            this.itemInfoLabel.Size = new System.Drawing.Size(67, 13);
            this.itemInfoLabel.TabIndex = 26;
            this.itemInfoLabel.Text = "Click an item";
            // 
            // CharacterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 544);
            this.Controls.Add(this.itemInfoLabel);
            this.Controls.Add(this.guildLabel);
            this.Controls.Add(this.classRaceLabel);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.rangedPictureBox);
            this.Controls.Add(this.offHandPictureBox);
            this.Controls.Add(this.mainHandPictureBox);
            this.Controls.Add(this.trinket2PictureBox);
            this.Controls.Add(this.trinket1PictureBox);
            this.Controls.Add(this.ring2PictureBox);
            this.Controls.Add(this.ring1PictureBox);
            this.Controls.Add(this.bootsPictureBox);
            this.Controls.Add(this.legsPictureBox);
            this.Controls.Add(this.beltPictureBox);
            this.Controls.Add(this.glovesPictureBox);
            this.Controls.Add(this.bracersPictureBox);
            this.Controls.Add(this.tabardPictureBox);
            this.Controls.Add(this.shirtPictureBox);
            this.Controls.Add(this.chestPictureBox);
            this.Controls.Add(this.cloakPictureBox);
            this.Controls.Add(this.shouldersPictureBox);
            this.Controls.Add(this.neckPictureBox);
            this.Controls.Add(this.helmPictureBox);
            this.Controls.Add(this.CharacterInfoLabel);
            this.Name = "CharacterForm";
            this.Text = "CharacterDisplay";
            ((System.ComponentModel.ISupportInitialize)(this.helmPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.neckPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shouldersPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cloakPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chestPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bracersPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabardPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shirtPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trinket2PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trinket1PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ring2PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ring1PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bootsPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.legsPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beltPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.glovesPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainHandPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offHandPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangedPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CharacterInfoLabel;
        private System.Windows.Forms.PictureBox helmPictureBox;
        private System.Windows.Forms.PictureBox neckPictureBox;
        private System.Windows.Forms.PictureBox shouldersPictureBox;
        private System.Windows.Forms.PictureBox cloakPictureBox;
        private System.Windows.Forms.PictureBox chestPictureBox;
        private System.Windows.Forms.PictureBox bracersPictureBox;
        private System.Windows.Forms.PictureBox tabardPictureBox;
        private System.Windows.Forms.PictureBox shirtPictureBox;
        private System.Windows.Forms.PictureBox trinket2PictureBox;
        private System.Windows.Forms.PictureBox trinket1PictureBox;
        private System.Windows.Forms.PictureBox ring2PictureBox;
        private System.Windows.Forms.PictureBox ring1PictureBox;
        private System.Windows.Forms.PictureBox bootsPictureBox;
        private System.Windows.Forms.PictureBox legsPictureBox;
        private System.Windows.Forms.PictureBox beltPictureBox;
        private System.Windows.Forms.PictureBox glovesPictureBox;
        private System.Windows.Forms.PictureBox mainHandPictureBox;
        private System.Windows.Forms.PictureBox offHandPictureBox;
        private System.Windows.Forms.PictureBox rangedPictureBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label classRaceLabel;
        private System.Windows.Forms.Label guildLabel;
        private System.Windows.Forms.Label itemInfoLabel;
    }
}