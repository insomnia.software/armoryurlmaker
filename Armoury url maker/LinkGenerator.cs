﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Armoury_url_maker
{
    public class LinkGenerator
    {
        private string mainURL = "http://eu.battle.net/wow/en/";
        string[] EURealms;
        string[] USRealms;

        public LinkGenerator()
        {
            string allEURealms = "Aegwynn,Aerie Peak,Agamaggan,Aggramar,Ahn'Qiraj,Al'Akir,Alexstrasza,Alleria,Alonsus,Aman'Thul,Ambossar,Anachronos,Anetheron,Antonidas,Anub'arak,Arak-arahm,Arathi,Arathor,Archimonde,Area 52,Argent Dawn,Arthas,Arygos,Aszune,Auchindoun,Azjol-Nerub,Azshara,Azuremyst,Baelgun,Balnazzar,Blackhand,Blackmoore,Blackrock,Blade's Edge,Bladefist,Bloodfeather,Bloodhoof,Bloodscalp,Blutkessel,Boulderfist,Bronze Dragonflight,Bronzebeard,Burning Blade,Burning Legion,Burning Steppes,C'Thun,Chamber of Aspects,Chants éternels,Cho'gall,Chromaggus,Colinas Pardas,Confrérie du Thorium,Conseil des Ombres,Crushridge,Culte de la Rive noire,Daggerspine,Dalaran,Dalvengyr,Darkmoon Faire,Darksorrow,Darkspear,Das Konsortium,Das Syndikat,Deathwing,Defias Brotherhood,Dentarg,Der abyssische Rat,Der Mithrilorden,Der Rat von Dalaran,Destromath,Dethecus,Die Aldor,Die Arguswacht,Die ewige Wacht,Die Nachtwache,Die Silberne Hand,Die Todeskrallen,Doomhammer,Draenor,Dragonblight,Dragonmaw,Drak'thul,Drek'Thar,Dun Modr,Dun Morogh,Dunemaul,Durotan,Earthen Ring,Echsenkessel,Eitrigg,Eldre'Thalas,Elune,Emerald Dream,Emeriss,Eonar,Eredar,Executus,Exodar,Festung der Stürme,Forscherliga,Frostmane,Frostmourne,Frostwhisper,Frostwolf,Garona,Garrosh,Genjuros,Ghostlands,Gilneas,Gorgonnash,Grim Batol,Gul'dan,Hakkar,Haomarush,Hellfire,Hellscream,Hyjal,Illidan,Jaedenar,Kael'thas,Karazhan,Kargath,Kazzak,Kel'Thuzad,Khadgar,Khaz Modan,Khaz'goroth,Kil'jaeden,Kilrogg,Kirin Tor,Kor'gall,Krag'jin,Krasus,Kul Tiras,Kult der Verdammten,La Croisade écarlate,Laughing Skull,Les Clairvoyants,Les Sentinelles,Lightbringer,Lightning's Blade,Lordaeron,Los Errantes,Lothar,Madmortem,The Maelstrom,Magtheridon,Mal'Ganis,Malfurion,Malorne,Malygos,Mannoroth,Marécage de Zangar,Mazrigos,Medivh,Minahonda,Moonglade,Mug'thol,Nagrand,Nathrezim,Naxxramas,Nazjatar,Nefarian,Neptulon,Ner'zhul,Nera'thor,Nethersturm,Nordrassil,Norgannon,Nozdormu,Onyxia,Outland,Perenolde,Proudmoore,Quel'Thalas,Ragnaros,Rajaxx,Rashgarroth,Ravencrest,Ravenholdt,Rexxar,Runetotem,Sanguino,Sargeras,Saurfang,Scarshield Legion,Sen'jin,The Sha'tar,Shadowsong,Shattered Halls,Shattered Hand,Shattrath,Shen'dralar,Silvermoon,Sinstralis.Skullcrusher,Spinebreaker,Sporeggar,Steamwheedle Cartel,Stormrage,Stormreaver,Stormscale,Sunstrider,Surama,Sylvanas,Taerar,Talnivarr,Tarren Mill,Teldrassil,Temple noir,Terenas,Terokkar,Terrordar,Theradras,Thrall,Throk'Feroth,Thunderhorn,Tichondrius,Tirion,Todeswache,Trollbane,Turalyon,Twilight's Hammer,Twisting Nether,Tyrande,Uldaman,Ulduar,Uldum,Un'Goro,Varimathras,Vashj,Vek'lor,Vek'nilash,The Venture Co,Vol'jin,Wildhammer,Wrathbringer,Xavius,Ysera,Ysondre,Zenedar,Zirkel des Cenarius,Zul'jin,Zuluhed,Азурегос,Борейская тундра,Вечная Песня,Галакронд,Гордунни,Гром,Дракономор,Король-лич,Пиратская бухта,Подземье,Разувий,Ревущий фьорд,Свежеватель Душ,Седогрив,Страж Смерти,Термоштепсель,Ткач Смерти,Черный Шрам,Ясеневый лес";
            EURealms = allEURealms.Split(',');

            string allUSRealms = "Aegwynn,Aerie Peak,Agamaggan,Aggramar,Akama,Alexstrasza,Alleria,Altar of Storms,Alterac Mountains,Aman'Thul,Andorhal,Anetheron,Antonidas,Anub'arak,Anvilmar,Arathor,Archimonde,Area 52,Argent Dawn,Arthas,Arygos,Auchindoun,Azgalor,Azjol-Nerub,Azshara,Azuremyst,Baelgun,Balnazzar,Barthilas,Black Dragonflight,Blackhand,Blackrock,Blackwater Raiders,Blackwing Lair,Blade's Edge,Bladefist,Bleeding Hollow,Blood Furnace,Bloodhoof,Bloodscalp,Bonechewer,Borean Tundra,Boulderfist,Bronzebeard,Burning Blade,Burning Legion,Caelestrasz,Cairne,Cenarion Circle,Cenarius,Cho'gall,Chromaggus,Coilfang,Crushridge,Daggerspine,Dalaran,Dalvengyr,Dark Iron,Darkspear,Darrowmere,Dath'Remar,Dawnbringer,Deathwing,Demon Soul,Dentarg,Destromath,Dethecus,Detheroc,Doomhammer,Draenor,Dragonblight,Dragonmaw,Drak'Tharon,Drak'thul,Draka,Drakkari,Dreadmaul,Drenden,Dunemaul,Durotan,Duskwood,Earthen Ring,Echo Isles,Eitrigg,Eldre'Thalas,Elune,Emerald Dream,Eonar,Eredar,Executus,Exodar,Farstriders,Feathermoon,Fenris,Firetree,Fizzcrank,The Forgotten Coast,Frostmane,Frostmourne,Frostwolf,Galakrond,Garithos,Garona,Garrosh,Ghostlands,Gilneas,Gnomeregan,Gorefiend,Gorgonnash,Greymane,Grizzly Hills,Gul'dan,Gundrak,Gurubashi,Hakkar,Haomarush,Hellscream,Hydraxis,Hyjal,Icecrown,Illidan,Jaedenar,Jubei'Thos,Kael'thas,Kalecgos,Kargath,Kel'Thuzad,Khadgar,Khaz Modan,Khaz'goroth,Kil'jaeden,Kilrogg,Kirin Tor,Korgath,Korialstrasz,Kul Tiras,Laughing Skull,Lethon,Lightbringer,Lightning's Blade,Lightninghoof,Llane,Lothar,Madoran,Maelstrom,Magtheridon,Maiev,Mal'Ganis,Malfurion,Malorne,Malygos,Mannoroth,Medivh,Misha,Mok'Nathal,Moon Guard,Moonrunner,Mug'thol,Muradin,Nagrand,Nathrezim,Nazgrel,Nazjatar,Ner'zhul,Nesingwary,Nordrassil,Norgannon,Onyxia,Perenolde,Proudmoore,Quel'dorei,Quel'Thalas,Ragnaros,Ravencrest,Ravenholdt,Rexxar,Rivendare,Runetotem,Sargeras,Saurfang,Scarlet Crusade,Scilla,The Scryers,Sen'jin,Sentinels,Shadow Council,Shadowmoon,Shadowsong,Shandris,Shattered Halls,Shattered Hand,Shu'halo,Silver Hand,Silvermoon,Sisters of Elune,Skullcrusher,Skywall,Smolderthorn,Spinebreaker,Spirestone,Staghelm,Steamwheedle Cartel,Stonemaul,Stormrage,Stormreaver,Stormscale,Suramar,Tanaris,Terenas,Terokkar,Thaurissan,Thorium Brotherhood,Thrall,Thunderhorn,Thunderlord,Tichondrius,Tortheldrin,Trollbane,Turalyon,Twisting Nether,Uldaman,Uldum,The Underbog,Undermine,Ursin,Uther,Vashj,Vek'nilash,Velen,The Venture Co,Warsong,Whisperwind,Wildhammer,Windrunner,Winterhoof,Wyrmrest Accord,Ysera,Ysondre,Zangarmarsh,Zul'jin,Zuluhed";
            USRealms = allUSRealms.Split(',');
        }

        public void SetUS()
        {
            mainURL = "http://us.battle.net/wow/en/";
        }

        public void SetEU()
        {
            mainURL = "http://eu.battle.net/wow/en/";
        }

        public string GenerateCharacterLink(string name, string realm)
        {
            string result = mainURL + "character/";

            result += (realm + "/" + name + "/");

            return result;
        }

        public string GenerateGuildLink(string name, string realm)
        {
            string result = mainURL + "guild/";

            result += (realm + "/" + name + "/");

            return result;
        }

        public string[] GetEURealms()
        {
            return EURealms;
        }

        public string[] GetUSRealms()
        {
            return USRealms;
        }
    }
}
