﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Armoury_url_maker
{
    public class Validation
    {
        public static string RealmValidation(string newRealm)
        {
            string validatedRealm = newRealm.ToLower();
            validatedRealm = validatedRealm.Replace("'", ""); //remove apostrphies
            validatedRealm = validatedRealm.Replace(" ", "-"); //spaces for hyphens
            return validatedRealm;
        }

        public static string NameValidation(string newName)
        {
            string validatedName = newName.ToLower();
            return validatedName;
        }

        public static string OldToNewConverter(string oldURL)
        {
            LinkGenerator TheLinkGenerator = new LinkGenerator();
            string realm = "";
            string name = "";
            if (oldURL.Substring(0, 1) == "h")
            {
                if (oldURL.Substring(25, 1) == "g")
                {
                    for (int i = 0; i < oldURL.Length - 1; i++)
                    {
                        string temp = "";
                        temp += oldURL[i] + oldURL[i+1];
                        if (oldURL[i] == 'r' && oldURL[i+1] == '=')
                        {
                            for (int j = i + 2; oldURL[j] == '&'; j++)
                            {
                                realm += oldURL[j];
                            }
                        }
                        if (oldURL[i] == 'n' && oldURL[i + 1] == '=')
                        {
                            for (int j = i + 2; j < oldURL.Length; j++)
                            {
                                name += oldURL[j];
                            }
                        }
                    }
                    return TheLinkGenerator.GenerateGuildLink(name, realm);
                }
                else if (oldURL.Substring(25, 1) == "c")
                {
                    return "character";
                }
                else
                {
                    return "";
                }
            }
            else if (oldURL.Substring(0, 1) == "w")
            {
                if (oldURL.Substring(18, 1) == "g")
                {
                    return "guild";
                }
                else if (oldURL.Substring(18, 1) == "c")
                {
                    return "character";
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }
    }
}
