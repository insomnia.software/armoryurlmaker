﻿namespace Armoury_url_maker
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.NameLabel = new System.Windows.Forms.Label();
            this.RealmLabel = new System.Windows.Forms.Label();
            this.RealmTextBox = new System.Windows.Forms.TextBox();
            this.LocationGroupBox = new System.Windows.Forms.GroupBox();
            this.USRadioButton = new System.Windows.Forms.RadioButton();
            this.EURadioButton = new System.Windows.Forms.RadioButton();
            this.URLTextBox = new System.Windows.Forms.TextBox();
            this.GenerateButton = new System.Windows.Forms.Button();
            this.URLLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.GuildRadioButton = new System.Windows.Forms.RadioButton();
            this.CharacterRadioButton = new System.Windows.Forms.RadioButton();
            this.RealmsListBox = new System.Windows.Forms.ListBox();
            this.CopyButton = new System.Windows.Forms.Button();
            this.AboutButton = new System.Windows.Forms.Button();
            this.ForignCharactersLabel = new System.Windows.Forms.Label();
            this.AGraveButton = new System.Windows.Forms.Button();
            this.EGraveButton = new System.Windows.Forms.Button();
            this.IGraveButton = new System.Windows.Forms.Button();
            this.OGraveButton = new System.Windows.Forms.Button();
            this.UGraveButton = new System.Windows.Forms.Button();
            this.YAcuteButton = new System.Windows.Forms.Button();
            this.UAcuteButton = new System.Windows.Forms.Button();
            this.OAcuteButton = new System.Windows.Forms.Button();
            this.IAcuteButton = new System.Windows.Forms.Button();
            this.EAcuteButton = new System.Windows.Forms.Button();
            this.AAcuteButton = new System.Windows.Forms.Button();
            this.UCircumflexButton = new System.Windows.Forms.Button();
            this.OCircumflexButton = new System.Windows.Forms.Button();
            this.ICircumflexButton = new System.Windows.Forms.Button();
            this.ECircumflexButton = new System.Windows.Forms.Button();
            this.ACircumflexButton = new System.Windows.Forms.Button();
            this.YUmlautButton = new System.Windows.Forms.Button();
            this.UUmlautButton = new System.Windows.Forms.Button();
            this.OUmlautButton = new System.Windows.Forms.Button();
            this.IUmlautButton = new System.Windows.Forms.Button();
            this.EUmlautButton = new System.Windows.Forms.Button();
            this.AUmlautButton = new System.Windows.Forms.Button();
            this.OTildeButton = new System.Windows.Forms.Button();
            this.NTildeButton = new System.Windows.Forms.Button();
            this.ATildeButton = new System.Windows.Forms.Button();
            this.SCzechButton = new System.Windows.Forms.Button();
            this.DIcelandicButton = new System.Windows.Forms.Button();
            this.ONordicButton = new System.Windows.Forms.Button();
            this.AEButton = new System.Windows.Forms.Button();
            this.CFrenchbutton = new System.Windows.Forms.Button();
            this.ANordicButton = new System.Windows.Forms.Button();
            this.ZCzechButton = new System.Windows.Forms.Button();
            this.OEButton = new System.Windows.Forms.Button();
            this.GetCharacterButton = new System.Windows.Forms.Button();
            this.LocationGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(63, 76);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(123, 20);
            this.NameTextBox.TabIndex = 3;
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(12, 79);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(35, 13);
            this.NameLabel.TabIndex = 2;
            this.NameLabel.Text = "Name";
            // 
            // RealmLabel
            // 
            this.RealmLabel.AutoSize = true;
            this.RealmLabel.Location = new System.Drawing.Point(12, 105);
            this.RealmLabel.Name = "RealmLabel";
            this.RealmLabel.Size = new System.Drawing.Size(37, 13);
            this.RealmLabel.TabIndex = 4;
            this.RealmLabel.Text = "Realm";
            // 
            // RealmTextBox
            // 
            this.RealmTextBox.Location = new System.Drawing.Point(63, 102);
            this.RealmTextBox.Name = "RealmTextBox";
            this.RealmTextBox.Size = new System.Drawing.Size(123, 20);
            this.RealmTextBox.TabIndex = 5;
            // 
            // LocationGroupBox
            // 
            this.LocationGroupBox.Controls.Add(this.USRadioButton);
            this.LocationGroupBox.Controls.Add(this.EURadioButton);
            this.LocationGroupBox.Location = new System.Drawing.Point(15, 20);
            this.LocationGroupBox.Name = "LocationGroupBox";
            this.LocationGroupBox.Size = new System.Drawing.Size(133, 35);
            this.LocationGroupBox.TabIndex = 0;
            this.LocationGroupBox.TabStop = false;
            // 
            // USRadioButton
            // 
            this.USRadioButton.AutoSize = true;
            this.USRadioButton.Location = new System.Drawing.Point(69, 12);
            this.USRadioButton.Name = "USRadioButton";
            this.USRadioButton.Size = new System.Drawing.Size(40, 17);
            this.USRadioButton.TabIndex = 1;
            this.USRadioButton.Text = "US";
            this.USRadioButton.UseVisualStyleBackColor = true;
            this.USRadioButton.CheckedChanged += new System.EventHandler(this.USRadioButton_CheckedChanged);
            // 
            // EURadioButton
            // 
            this.EURadioButton.AutoSize = true;
            this.EURadioButton.Checked = true;
            this.EURadioButton.Location = new System.Drawing.Point(7, 12);
            this.EURadioButton.Name = "EURadioButton";
            this.EURadioButton.Size = new System.Drawing.Size(40, 17);
            this.EURadioButton.TabIndex = 0;
            this.EURadioButton.TabStop = true;
            this.EURadioButton.Text = "EU";
            this.EURadioButton.UseVisualStyleBackColor = true;
            // 
            // URLTextBox
            // 
            this.URLTextBox.Location = new System.Drawing.Point(12, 399);
            this.URLTextBox.Name = "URLTextBox";
            this.URLTextBox.Size = new System.Drawing.Size(373, 20);
            this.URLTextBox.TabIndex = 11;
            // 
            // GenerateButton
            // 
            this.GenerateButton.Location = new System.Drawing.Point(12, 352);
            this.GenerateButton.Name = "GenerateButton";
            this.GenerateButton.Size = new System.Drawing.Size(75, 23);
            this.GenerateButton.TabIndex = 7;
            this.GenerateButton.Text = "Generate";
            this.GenerateButton.UseVisualStyleBackColor = true;
            this.GenerateButton.Click += new System.EventHandler(this.GenerateButton_Click);
            // 
            // URLLabel
            // 
            this.URLLabel.AutoSize = true;
            this.URLLabel.Location = new System.Drawing.Point(11, 383);
            this.URLLabel.Name = "URLLabel";
            this.URLLabel.Size = new System.Drawing.Size(29, 13);
            this.URLLabel.TabIndex = 10;
            this.URLLabel.Text = "URL";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.GuildRadioButton);
            this.groupBox1.Controls.Add(this.CharacterRadioButton);
            this.groupBox1.Location = new System.Drawing.Point(250, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(131, 35);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // GuildRadioButton
            // 
            this.GuildRadioButton.AutoSize = true;
            this.GuildRadioButton.Location = new System.Drawing.Point(82, 13);
            this.GuildRadioButton.Name = "GuildRadioButton";
            this.GuildRadioButton.Size = new System.Drawing.Size(49, 17);
            this.GuildRadioButton.TabIndex = 1;
            this.GuildRadioButton.Text = "Guild";
            this.GuildRadioButton.UseVisualStyleBackColor = true;
            // 
            // CharacterRadioButton
            // 
            this.CharacterRadioButton.AutoSize = true;
            this.CharacterRadioButton.Checked = true;
            this.CharacterRadioButton.Location = new System.Drawing.Point(5, 13);
            this.CharacterRadioButton.Name = "CharacterRadioButton";
            this.CharacterRadioButton.Size = new System.Drawing.Size(71, 17);
            this.CharacterRadioButton.TabIndex = 0;
            this.CharacterRadioButton.TabStop = true;
            this.CharacterRadioButton.Text = "Character";
            this.CharacterRadioButton.UseVisualStyleBackColor = true;
            // 
            // RealmsListBox
            // 
            this.RealmsListBox.FormattingEnabled = true;
            this.RealmsListBox.Location = new System.Drawing.Point(205, 76);
            this.RealmsListBox.Name = "RealmsListBox";
            this.RealmsListBox.Size = new System.Drawing.Size(176, 251);
            this.RealmsListBox.TabIndex = 6;
            this.RealmsListBox.SelectedIndexChanged += new System.EventHandler(this.RealmsListBox_SelectedIndexChanged);
            // 
            // CopyButton
            // 
            this.CopyButton.Location = new System.Drawing.Point(108, 352);
            this.CopyButton.Name = "CopyButton";
            this.CopyButton.Size = new System.Drawing.Size(75, 23);
            this.CopyButton.TabIndex = 8;
            this.CopyButton.Text = "Copy URL";
            this.CopyButton.UseVisualStyleBackColor = true;
            this.CopyButton.Click += new System.EventHandler(this.CopyButton_Click);
            // 
            // AboutButton
            // 
            this.AboutButton.Location = new System.Drawing.Point(306, 352);
            this.AboutButton.Name = "AboutButton";
            this.AboutButton.Size = new System.Drawing.Size(75, 23);
            this.AboutButton.TabIndex = 9;
            this.AboutButton.Text = "About";
            this.AboutButton.UseVisualStyleBackColor = true;
            this.AboutButton.Click += new System.EventHandler(this.AboutButton_Click);
            // 
            // ForignCharactersLabel
            // 
            this.ForignCharactersLabel.AutoSize = true;
            this.ForignCharactersLabel.Location = new System.Drawing.Point(9, 139);
            this.ForignCharactersLabel.Name = "ForignCharactersLabel";
            this.ForignCharactersLabel.Size = new System.Drawing.Size(89, 13);
            this.ForignCharactersLabel.TabIndex = 12;
            this.ForignCharactersLabel.Text = "Forign characters";
            // 
            // AGraveButton
            // 
            this.AGraveButton.Location = new System.Drawing.Point(12, 155);
            this.AGraveButton.Name = "AGraveButton";
            this.AGraveButton.Size = new System.Drawing.Size(23, 23);
            this.AGraveButton.TabIndex = 13;
            this.AGraveButton.Text = "à";
            this.AGraveButton.UseVisualStyleBackColor = true;
            this.AGraveButton.Click += new System.EventHandler(this.AGraveButton_Click);
            // 
            // EGraveButton
            // 
            this.EGraveButton.Location = new System.Drawing.Point(41, 155);
            this.EGraveButton.Name = "EGraveButton";
            this.EGraveButton.Size = new System.Drawing.Size(23, 23);
            this.EGraveButton.TabIndex = 14;
            this.EGraveButton.Text = "è";
            this.EGraveButton.UseVisualStyleBackColor = true;
            this.EGraveButton.Click += new System.EventHandler(this.EGraveButton_Click);
            // 
            // IGraveButton
            // 
            this.IGraveButton.Location = new System.Drawing.Point(70, 155);
            this.IGraveButton.Name = "IGraveButton";
            this.IGraveButton.Size = new System.Drawing.Size(23, 23);
            this.IGraveButton.TabIndex = 15;
            this.IGraveButton.Text = "ì";
            this.IGraveButton.UseVisualStyleBackColor = true;
            this.IGraveButton.Click += new System.EventHandler(this.IGraveButton_Click);
            // 
            // OGraveButton
            // 
            this.OGraveButton.Location = new System.Drawing.Point(99, 155);
            this.OGraveButton.Name = "OGraveButton";
            this.OGraveButton.Size = new System.Drawing.Size(23, 23);
            this.OGraveButton.TabIndex = 16;
            this.OGraveButton.Text = "ò";
            this.OGraveButton.UseVisualStyleBackColor = true;
            this.OGraveButton.Click += new System.EventHandler(this.OGraveButton_Click);
            // 
            // UGraveButton
            // 
            this.UGraveButton.Location = new System.Drawing.Point(128, 155);
            this.UGraveButton.Name = "UGraveButton";
            this.UGraveButton.Size = new System.Drawing.Size(23, 23);
            this.UGraveButton.TabIndex = 17;
            this.UGraveButton.Text = "ù";
            this.UGraveButton.UseVisualStyleBackColor = true;
            this.UGraveButton.Click += new System.EventHandler(this.UGraveButton_Click);
            // 
            // YAcuteButton
            // 
            this.YAcuteButton.Location = new System.Drawing.Point(157, 184);
            this.YAcuteButton.Name = "YAcuteButton";
            this.YAcuteButton.Size = new System.Drawing.Size(23, 23);
            this.YAcuteButton.TabIndex = 23;
            this.YAcuteButton.Text = "ý";
            this.YAcuteButton.UseVisualStyleBackColor = true;
            this.YAcuteButton.Click += new System.EventHandler(this.YAcuteButton_Click);
            // 
            // UAcuteButton
            // 
            this.UAcuteButton.Location = new System.Drawing.Point(128, 184);
            this.UAcuteButton.Name = "UAcuteButton";
            this.UAcuteButton.Size = new System.Drawing.Size(23, 23);
            this.UAcuteButton.TabIndex = 22;
            this.UAcuteButton.Text = "ú";
            this.UAcuteButton.UseVisualStyleBackColor = true;
            this.UAcuteButton.Click += new System.EventHandler(this.UAcuteButton_Click);
            // 
            // OAcuteButton
            // 
            this.OAcuteButton.Location = new System.Drawing.Point(99, 184);
            this.OAcuteButton.Name = "OAcuteButton";
            this.OAcuteButton.Size = new System.Drawing.Size(23, 23);
            this.OAcuteButton.TabIndex = 21;
            this.OAcuteButton.Text = "ó";
            this.OAcuteButton.UseVisualStyleBackColor = true;
            this.OAcuteButton.Click += new System.EventHandler(this.OAcuteButton_Click);
            // 
            // IAcuteButton
            // 
            this.IAcuteButton.Location = new System.Drawing.Point(70, 184);
            this.IAcuteButton.Name = "IAcuteButton";
            this.IAcuteButton.Size = new System.Drawing.Size(23, 23);
            this.IAcuteButton.TabIndex = 20;
            this.IAcuteButton.Text = "í";
            this.IAcuteButton.UseVisualStyleBackColor = true;
            this.IAcuteButton.Click += new System.EventHandler(this.IAcuteButton_Click);
            // 
            // EAcuteButton
            // 
            this.EAcuteButton.Location = new System.Drawing.Point(41, 184);
            this.EAcuteButton.Name = "EAcuteButton";
            this.EAcuteButton.Size = new System.Drawing.Size(23, 23);
            this.EAcuteButton.TabIndex = 19;
            this.EAcuteButton.Text = "é";
            this.EAcuteButton.UseVisualStyleBackColor = true;
            this.EAcuteButton.Click += new System.EventHandler(this.EAcuteButton_Click);
            // 
            // AAcuteButton
            // 
            this.AAcuteButton.Location = new System.Drawing.Point(12, 184);
            this.AAcuteButton.Name = "AAcuteButton";
            this.AAcuteButton.Size = new System.Drawing.Size(23, 23);
            this.AAcuteButton.TabIndex = 18;
            this.AAcuteButton.Text = "á";
            this.AAcuteButton.UseVisualStyleBackColor = true;
            this.AAcuteButton.Click += new System.EventHandler(this.AAcuteButton_Click);
            // 
            // UCircumflexButton
            // 
            this.UCircumflexButton.Location = new System.Drawing.Point(128, 213);
            this.UCircumflexButton.Name = "UCircumflexButton";
            this.UCircumflexButton.Size = new System.Drawing.Size(23, 23);
            this.UCircumflexButton.TabIndex = 28;
            this.UCircumflexButton.Text = "û";
            this.UCircumflexButton.UseVisualStyleBackColor = true;
            this.UCircumflexButton.Click += new System.EventHandler(this.UCircumflexButton_Click);
            // 
            // OCircumflexButton
            // 
            this.OCircumflexButton.Location = new System.Drawing.Point(99, 213);
            this.OCircumflexButton.Name = "OCircumflexButton";
            this.OCircumflexButton.Size = new System.Drawing.Size(23, 23);
            this.OCircumflexButton.TabIndex = 27;
            this.OCircumflexButton.Text = "ô";
            this.OCircumflexButton.UseVisualStyleBackColor = true;
            this.OCircumflexButton.Click += new System.EventHandler(this.OCircumflexButton_Click);
            // 
            // ICircumflexButton
            // 
            this.ICircumflexButton.Location = new System.Drawing.Point(70, 213);
            this.ICircumflexButton.Name = "ICircumflexButton";
            this.ICircumflexButton.Size = new System.Drawing.Size(23, 23);
            this.ICircumflexButton.TabIndex = 26;
            this.ICircumflexButton.Text = "î";
            this.ICircumflexButton.UseVisualStyleBackColor = true;
            this.ICircumflexButton.Click += new System.EventHandler(this.ICircumflexButton_Click);
            // 
            // ECircumflexButton
            // 
            this.ECircumflexButton.Location = new System.Drawing.Point(41, 213);
            this.ECircumflexButton.Name = "ECircumflexButton";
            this.ECircumflexButton.Size = new System.Drawing.Size(23, 23);
            this.ECircumflexButton.TabIndex = 25;
            this.ECircumflexButton.Text = "ê";
            this.ECircumflexButton.UseVisualStyleBackColor = true;
            this.ECircumflexButton.Click += new System.EventHandler(this.ECircumflexButton_Click);
            // 
            // ACircumflexButton
            // 
            this.ACircumflexButton.Location = new System.Drawing.Point(12, 213);
            this.ACircumflexButton.Name = "ACircumflexButton";
            this.ACircumflexButton.Size = new System.Drawing.Size(23, 23);
            this.ACircumflexButton.TabIndex = 24;
            this.ACircumflexButton.Text = "â";
            this.ACircumflexButton.UseVisualStyleBackColor = true;
            this.ACircumflexButton.Click += new System.EventHandler(this.ACircumflexButton_Click);
            // 
            // YUmlautButton
            // 
            this.YUmlautButton.Location = new System.Drawing.Point(157, 242);
            this.YUmlautButton.Name = "YUmlautButton";
            this.YUmlautButton.Size = new System.Drawing.Size(23, 23);
            this.YUmlautButton.TabIndex = 34;
            this.YUmlautButton.Text = "ÿ";
            this.YUmlautButton.UseVisualStyleBackColor = true;
            this.YUmlautButton.Click += new System.EventHandler(this.YUmlautButton_Click);
            // 
            // UUmlautButton
            // 
            this.UUmlautButton.Location = new System.Drawing.Point(128, 242);
            this.UUmlautButton.Name = "UUmlautButton";
            this.UUmlautButton.Size = new System.Drawing.Size(23, 23);
            this.UUmlautButton.TabIndex = 33;
            this.UUmlautButton.Text = "ü";
            this.UUmlautButton.UseVisualStyleBackColor = true;
            this.UUmlautButton.Click += new System.EventHandler(this.UUmlautButton_Click);
            // 
            // OUmlautButton
            // 
            this.OUmlautButton.Location = new System.Drawing.Point(99, 242);
            this.OUmlautButton.Name = "OUmlautButton";
            this.OUmlautButton.Size = new System.Drawing.Size(23, 23);
            this.OUmlautButton.TabIndex = 32;
            this.OUmlautButton.Text = "ö";
            this.OUmlautButton.UseVisualStyleBackColor = true;
            this.OUmlautButton.Click += new System.EventHandler(this.OUmlautButton_Click);
            // 
            // IUmlautButton
            // 
            this.IUmlautButton.Location = new System.Drawing.Point(70, 242);
            this.IUmlautButton.Name = "IUmlautButton";
            this.IUmlautButton.Size = new System.Drawing.Size(23, 23);
            this.IUmlautButton.TabIndex = 31;
            this.IUmlautButton.Text = "ï";
            this.IUmlautButton.UseVisualStyleBackColor = true;
            this.IUmlautButton.Click += new System.EventHandler(this.IUmlautButton_Click);
            // 
            // EUmlautButton
            // 
            this.EUmlautButton.Location = new System.Drawing.Point(41, 242);
            this.EUmlautButton.Name = "EUmlautButton";
            this.EUmlautButton.Size = new System.Drawing.Size(23, 23);
            this.EUmlautButton.TabIndex = 30;
            this.EUmlautButton.Text = "ë";
            this.EUmlautButton.UseVisualStyleBackColor = true;
            this.EUmlautButton.Click += new System.EventHandler(this.EUmlautButton_Click);
            // 
            // AUmlautButton
            // 
            this.AUmlautButton.Location = new System.Drawing.Point(12, 242);
            this.AUmlautButton.Name = "AUmlautButton";
            this.AUmlautButton.Size = new System.Drawing.Size(23, 23);
            this.AUmlautButton.TabIndex = 29;
            this.AUmlautButton.Text = "ä";
            this.AUmlautButton.UseVisualStyleBackColor = true;
            this.AUmlautButton.Click += new System.EventHandler(this.AUmlautButton_Click);
            // 
            // OTildeButton
            // 
            this.OTildeButton.Location = new System.Drawing.Point(99, 271);
            this.OTildeButton.Name = "OTildeButton";
            this.OTildeButton.Size = new System.Drawing.Size(23, 23);
            this.OTildeButton.TabIndex = 38;
            this.OTildeButton.Text = "õ";
            this.OTildeButton.UseVisualStyleBackColor = true;
            this.OTildeButton.Click += new System.EventHandler(this.OTildeButton_Click);
            // 
            // NTildeButton
            // 
            this.NTildeButton.Location = new System.Drawing.Point(70, 271);
            this.NTildeButton.Name = "NTildeButton";
            this.NTildeButton.Size = new System.Drawing.Size(23, 23);
            this.NTildeButton.TabIndex = 37;
            this.NTildeButton.Text = "ñ";
            this.NTildeButton.UseVisualStyleBackColor = true;
            this.NTildeButton.Click += new System.EventHandler(this.NTildeButton_Click);
            // 
            // ATildeButton
            // 
            this.ATildeButton.Location = new System.Drawing.Point(12, 271);
            this.ATildeButton.Name = "ATildeButton";
            this.ATildeButton.Size = new System.Drawing.Size(23, 23);
            this.ATildeButton.TabIndex = 35;
            this.ATildeButton.Text = "ã";
            this.ATildeButton.UseVisualStyleBackColor = true;
            this.ATildeButton.Click += new System.EventHandler(this.ATildeButton_Click);
            // 
            // SCzechButton
            // 
            this.SCzechButton.Location = new System.Drawing.Point(128, 271);
            this.SCzechButton.Name = "SCzechButton";
            this.SCzechButton.Size = new System.Drawing.Size(23, 23);
            this.SCzechButton.TabIndex = 39;
            this.SCzechButton.Text = "š";
            this.SCzechButton.UseVisualStyleBackColor = true;
            this.SCzechButton.Click += new System.EventHandler(this.SCzechButton_Click);
            // 
            // DIcelandicButton
            // 
            this.DIcelandicButton.Location = new System.Drawing.Point(128, 300);
            this.DIcelandicButton.Name = "DIcelandicButton";
            this.DIcelandicButton.Size = new System.Drawing.Size(23, 23);
            this.DIcelandicButton.TabIndex = 45;
            this.DIcelandicButton.Text = "Ð";
            this.DIcelandicButton.UseVisualStyleBackColor = true;
            this.DIcelandicButton.Click += new System.EventHandler(this.DIcelandicButton_Click);
            // 
            // ONordicButton
            // 
            this.ONordicButton.Location = new System.Drawing.Point(99, 300);
            this.ONordicButton.Name = "ONordicButton";
            this.ONordicButton.Size = new System.Drawing.Size(23, 23);
            this.ONordicButton.TabIndex = 44;
            this.ONordicButton.Text = "ø";
            this.ONordicButton.UseVisualStyleBackColor = true;
            this.ONordicButton.Click += new System.EventHandler(this.ONordicButton_Click);
            // 
            // AEButton
            // 
            this.AEButton.Location = new System.Drawing.Point(41, 271);
            this.AEButton.Name = "AEButton";
            this.AEButton.Size = new System.Drawing.Size(23, 23);
            this.AEButton.TabIndex = 36;
            this.AEButton.Text = "æ";
            this.AEButton.UseVisualStyleBackColor = true;
            this.AEButton.Click += new System.EventHandler(this.AEButton_Click);
            // 
            // CFrenchbutton
            // 
            this.CFrenchbutton.Location = new System.Drawing.Point(70, 300);
            this.CFrenchbutton.Name = "CFrenchbutton";
            this.CFrenchbutton.Size = new System.Drawing.Size(23, 23);
            this.CFrenchbutton.TabIndex = 43;
            this.CFrenchbutton.Text = "ç";
            this.CFrenchbutton.UseVisualStyleBackColor = true;
            this.CFrenchbutton.Click += new System.EventHandler(this.CFrenchbutton_Click);
            // 
            // ANordicButton
            // 
            this.ANordicButton.Location = new System.Drawing.Point(12, 300);
            this.ANordicButton.Name = "ANordicButton";
            this.ANordicButton.Size = new System.Drawing.Size(23, 23);
            this.ANordicButton.TabIndex = 41;
            this.ANordicButton.Text = "å";
            this.ANordicButton.UseVisualStyleBackColor = true;
            this.ANordicButton.Click += new System.EventHandler(this.ANordicButton_Click);
            // 
            // ZCzechButton
            // 
            this.ZCzechButton.Location = new System.Drawing.Point(157, 271);
            this.ZCzechButton.Name = "ZCzechButton";
            this.ZCzechButton.Size = new System.Drawing.Size(23, 23);
            this.ZCzechButton.TabIndex = 40;
            this.ZCzechButton.Text = "ž";
            this.ZCzechButton.UseVisualStyleBackColor = true;
            this.ZCzechButton.Click += new System.EventHandler(this.ZCzechButton_Click);
            // 
            // OEButton
            // 
            this.OEButton.Location = new System.Drawing.Point(41, 300);
            this.OEButton.Name = "OEButton";
            this.OEButton.Size = new System.Drawing.Size(23, 23);
            this.OEButton.TabIndex = 42;
            this.OEButton.Text = "œ";
            this.OEButton.UseVisualStyleBackColor = true;
            this.OEButton.Click += new System.EventHandler(this.OEButton_Click);
            // 
            // GetCharacterButton
            // 
            this.GetCharacterButton.Location = new System.Drawing.Point(14, 436);
            this.GetCharacterButton.Name = "GetCharacterButton";
            this.GetCharacterButton.Size = new System.Drawing.Size(108, 23);
            this.GetCharacterButton.TabIndex = 46;
            this.GetCharacterButton.Text = "Get Character";
            this.GetCharacterButton.UseVisualStyleBackColor = true;
            this.GetCharacterButton.Click += new System.EventHandler(this.GetCharacterButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 471);
            this.Controls.Add(this.GetCharacterButton);
            this.Controls.Add(this.OEButton);
            this.Controls.Add(this.ZCzechButton);
            this.Controls.Add(this.SCzechButton);
            this.Controls.Add(this.DIcelandicButton);
            this.Controls.Add(this.ONordicButton);
            this.Controls.Add(this.AEButton);
            this.Controls.Add(this.CFrenchbutton);
            this.Controls.Add(this.ANordicButton);
            this.Controls.Add(this.OTildeButton);
            this.Controls.Add(this.NTildeButton);
            this.Controls.Add(this.ATildeButton);
            this.Controls.Add(this.YUmlautButton);
            this.Controls.Add(this.UUmlautButton);
            this.Controls.Add(this.OUmlautButton);
            this.Controls.Add(this.IUmlautButton);
            this.Controls.Add(this.EUmlautButton);
            this.Controls.Add(this.AUmlautButton);
            this.Controls.Add(this.UCircumflexButton);
            this.Controls.Add(this.OCircumflexButton);
            this.Controls.Add(this.ICircumflexButton);
            this.Controls.Add(this.ECircumflexButton);
            this.Controls.Add(this.ACircumflexButton);
            this.Controls.Add(this.YAcuteButton);
            this.Controls.Add(this.UAcuteButton);
            this.Controls.Add(this.OAcuteButton);
            this.Controls.Add(this.IAcuteButton);
            this.Controls.Add(this.EAcuteButton);
            this.Controls.Add(this.AAcuteButton);
            this.Controls.Add(this.UGraveButton);
            this.Controls.Add(this.OGraveButton);
            this.Controls.Add(this.IGraveButton);
            this.Controls.Add(this.EGraveButton);
            this.Controls.Add(this.AGraveButton);
            this.Controls.Add(this.ForignCharactersLabel);
            this.Controls.Add(this.AboutButton);
            this.Controls.Add(this.CopyButton);
            this.Controls.Add(this.RealmsListBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.URLLabel);
            this.Controls.Add(this.GenerateButton);
            this.Controls.Add(this.URLTextBox);
            this.Controls.Add(this.LocationGroupBox);
            this.Controls.Add(this.RealmLabel);
            this.Controls.Add(this.RealmTextBox);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.NameTextBox);
            this.Name = "MainForm";
            this.Text = "Armoury Link Generator";
            this.LocationGroupBox.ResumeLayout(false);
            this.LocationGroupBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Label RealmLabel;
        private System.Windows.Forms.TextBox RealmTextBox;
        private System.Windows.Forms.GroupBox LocationGroupBox;
        private System.Windows.Forms.RadioButton USRadioButton;
        private System.Windows.Forms.RadioButton EURadioButton;
        private System.Windows.Forms.TextBox URLTextBox;
        private System.Windows.Forms.Button GenerateButton;
        private System.Windows.Forms.Label URLLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton GuildRadioButton;
        private System.Windows.Forms.RadioButton CharacterRadioButton;
        private System.Windows.Forms.ListBox RealmsListBox;
        private System.Windows.Forms.Button CopyButton;
        private System.Windows.Forms.Button AboutButton;
        private System.Windows.Forms.Label ForignCharactersLabel;
        private System.Windows.Forms.Button AGraveButton;
        private System.Windows.Forms.Button EGraveButton;
        private System.Windows.Forms.Button IGraveButton;
        private System.Windows.Forms.Button OGraveButton;
        private System.Windows.Forms.Button UGraveButton;
        private System.Windows.Forms.Button YAcuteButton;
        private System.Windows.Forms.Button UAcuteButton;
        private System.Windows.Forms.Button OAcuteButton;
        private System.Windows.Forms.Button IAcuteButton;
        private System.Windows.Forms.Button EAcuteButton;
        private System.Windows.Forms.Button AAcuteButton;
        private System.Windows.Forms.Button UCircumflexButton;
        private System.Windows.Forms.Button OCircumflexButton;
        private System.Windows.Forms.Button ICircumflexButton;
        private System.Windows.Forms.Button ECircumflexButton;
        private System.Windows.Forms.Button ACircumflexButton;
        private System.Windows.Forms.Button YUmlautButton;
        private System.Windows.Forms.Button UUmlautButton;
        private System.Windows.Forms.Button OUmlautButton;
        private System.Windows.Forms.Button IUmlautButton;
        private System.Windows.Forms.Button EUmlautButton;
        private System.Windows.Forms.Button AUmlautButton;
        private System.Windows.Forms.Button OTildeButton;
        private System.Windows.Forms.Button NTildeButton;
        private System.Windows.Forms.Button ATildeButton;
        private System.Windows.Forms.Button SCzechButton;
        private System.Windows.Forms.Button DIcelandicButton;
        private System.Windows.Forms.Button ONordicButton;
        private System.Windows.Forms.Button AEButton;
        private System.Windows.Forms.Button CFrenchbutton;
        private System.Windows.Forms.Button ANordicButton;
        private System.Windows.Forms.Button ZCzechButton;
        private System.Windows.Forms.Button OEButton;
        private System.Windows.Forms.Button GetCharacterButton;
    }
}

