﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Armoury_url_maker
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            LinkGenerator TheLinkGenerator = new LinkGenerator(); 
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm(TheLinkGenerator));
        }
    }
}
