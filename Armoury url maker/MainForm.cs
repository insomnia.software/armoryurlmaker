﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Armoury_url_maker
{
    public partial class MainForm : Form
    {
        LinkGenerator TheLinkGenerator;

        public MainForm(LinkGenerator NewLinkGenerator)
        {
            TheLinkGenerator = NewLinkGenerator;
            InitializeComponent();

            RealmsListBox.DataSource = TheLinkGenerator.GetEURealms();
        }

        private void GenerateButton_Click(object sender, EventArgs e)
        {
            if (NameTextBox.Text == "")
            {
                MessageBox.Show("No name entered", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                string realm = Validation.RealmValidation(RealmTextBox.Text);
                string name = Validation.NameValidation(NameTextBox.Text);

                if (CharacterRadioButton.Checked)
                {
                    URLTextBox.Text = TheLinkGenerator.GenerateCharacterLink(name, realm);
                }
                else
                {
                    URLTextBox.Text = TheLinkGenerator.GenerateGuildLink(name, realm);
                }
            }
        }

        private void USRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (EURadioButton.Checked)
            {
                //changing to eu
                TheLinkGenerator.SetEU();
                RealmsListBox.DataSource = TheLinkGenerator.GetEURealms();
            }
            else
            {
                TheLinkGenerator.SetUS();
                RealmsListBox.DataSource = TheLinkGenerator.GetUSRealms();
            }
        }

        private void RealmsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            RealmTextBox.Text = (string)RealmsListBox.SelectedItem;
        }

        private void CopyButton_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetText(URLTextBox.Text);
            }
            catch
            {
                MessageBox.Show("Error copying to clipboard\n\nTry updating .NET", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AboutButton_Click(object sender, EventArgs e)
        {
            string about = "Made by Insomnia of Serendipity\n\nPlease report bugs or requested features on the forum thread";
            MessageBox.Show(about, "Armoury Link Generator v0.4", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        //private void ConvertButton_Click(object sender, EventArgs e)
        //{
        //    if (OldURLTextBox.Text == "")
        //    {
        //        MessageBox.Show("No URL entered", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //    else
        //    {
        //        NewURLTextBox.Text = Validation.OldToNewConverter(OldURLTextBox.Text);
        //    }
        //}

        private void AGraveButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "à";
            NameTextBox.Select();
        }

        private void EGraveButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "è";
            NameTextBox.Select();
        }

        private void IGraveButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ì";
            NameTextBox.Select();
        }

        private void OGraveButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ò";
            NameTextBox.Select();
        }

        private void UGraveButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ù";
            NameTextBox.Select();
        }

        private void AAcuteButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "á";
            NameTextBox.Select();
        }

        private void EAcuteButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "é";
            NameTextBox.Select();
        }

        private void IAcuteButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "í";
            NameTextBox.Select();
        }

        private void OAcuteButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ó";
            NameTextBox.Select();
        }

        private void UAcuteButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ú";
            NameTextBox.Select();
        }

        private void YAcuteButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ý";
            NameTextBox.Select();
        }

        private void ACircumflexButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "â";
            NameTextBox.Select();
        }

        private void ECircumflexButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ê";
            NameTextBox.Select();
        }

        private void ICircumflexButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "î";
            NameTextBox.Select();
        }

        private void OCircumflexButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ô";
            NameTextBox.Select();
        }

        private void UCircumflexButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "û";
            NameTextBox.Select();
        }

        private void AUmlautButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ä";
            NameTextBox.Select();
        }

        private void EUmlautButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ë";
            NameTextBox.Select();
        }

        private void IUmlautButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ï";
            NameTextBox.Select();
        }

        private void OUmlautButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ö";
            NameTextBox.Select();
        }

        private void UUmlautButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ü";
            NameTextBox.Select();
        }

        private void YUmlautButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ÿ";
            NameTextBox.Select();
        }

        private void ATildeButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ã";
            NameTextBox.Select();
        }

        private void NTildeButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ñ";
            NameTextBox.Select();
        }

        private void OTildeButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "õ";
            NameTextBox.Select();
        }

        private void AEButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "æ";
            NameTextBox.Select();
        }

        private void SCzechButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "š";
            NameTextBox.Select();
        }

        private void ZCzechButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ž";
            NameTextBox.Select();
        }

        private void ANordicButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "å";
            NameTextBox.Select();
        }

        private void OEButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "œ";
            NameTextBox.Select();
        }

        private void CFrenchbutton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ç";
            NameTextBox.Select();
        }

        private void ONordicButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "ø";
            NameTextBox.Select();
        }

        private void DIcelandicButton_Click(object sender, EventArgs e)
        {
            NameTextBox.SelectedText = "Ð";
            NameTextBox.Select();
        }

        private void GetCharacterButton_Click(object sender, EventArgs e)
        {
            if (URLTextBox.Text != "")
            {
                Character character = new Character(NameTextBox.Text, RealmTextBox.Text);
                if (character.GetHTML(URLTextBox.Text))
                {
                    //try
                    //{
                        character.ParseHTML();
                    //}
                    //catch
                    //{
                    //    MessageBox.Show("HTML parsing failed\n\nPlease report the bug", "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //}
                    //spawn new form, pass it the character
                    CharacterForm TheCharacterForm = new CharacterForm(character);
                    TheCharacterForm.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Page not found", "Error 404", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                MessageBox.Show("No URL", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
