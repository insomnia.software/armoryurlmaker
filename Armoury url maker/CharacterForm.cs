﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Armoury_url_maker
{
    public partial class CharacterForm : Form
    {
        Character theCharacter;

        public CharacterForm(Character character)
        {
            theCharacter = character;

            InitializeComponent();

            nameLabel.Text = character._name;
            guildLabel.Text = "<" + character._guild + ">" + " of " + character._realm;
            classRaceLabel.Text = character._level + " " + character._race + " " + character._spec + " " + character._class;
            classRaceLabel.Location = new Point(336 - classRaceLabel.Width - 9, 29);
            titleLabel.Text = character._title;
            titleLabel.Location = new Point(336 - titleLabel.Width - 9, 9);

            CharacterInfoLabel.Text = character.ToString();

            //pre loading, make this an option
            //helmPictureBox.Image = character.items[0]._itemImage;
            //neckPictureBox.Image = character.items[1]._itemImage;
            //shouldersPictureBox.Image = character.items[2]._itemImage;
            //cloakPictureBox.Image = character.items[3]._itemImage;
            //chestPictureBox.Image = character.items[4]._itemImage;
            //shirtPictureBox.Image = character.items[5]._itemImage;
            //tabardPictureBox.Image = character.items[6]._itemImage;
            //bracersPictureBox.Image = character.items[7]._itemImage;
            //glovesPictureBox.Image = character.items[8]._itemImage;
            //beltPictureBox.Image = character.items[9]._itemImage;
            //legsPictureBox.Image = character.items[10]._itemImage;
            //bootsPictureBox.Image = character.items[11]._itemImage;
            //ring1PictureBox.Image = character.items[12]._itemImage;
            //ring2PictureBox.Image = character.items[13]._itemImage;
            //trinket1PictureBox.Image = character.items[14]._itemImage;
            //trinket2PictureBox.Image = character.items[15]._itemImage;
            //mainHandPictureBox.Image = character.items[16]._itemImage;
            //offHandPictureBox.Image = character.items[17]._itemImage;
            //rangedPictureBox.Image = character.items[18]._itemImage;

            //helmPictureBox.ImageLocation = character.items[0]._wowHeadImageURL;
            //neckPictureBox.ImageLocation = character.items[1]._wowHeadImageURL;
            //shouldersPictureBox.ImageLocation = character.items[2]._wowHeadImageURL;
            //cloakPictureBox.ImageLocation = character.items[3]._wowHeadImageURL;
            //chestPictureBox.ImageLocation = character.items[4]._wowHeadImageURL;
            //shirtPictureBox.ImageLocation = character.items[5]._wowHeadImageURL;
            //tabardPictureBox.ImageLocation = character.items[6]._wowHeadImageURL;
            //bracersPictureBox.ImageLocation = character.items[7]._wowHeadImageURL;
            //glovesPictureBox.ImageLocation = character.items[8]._wowHeadImageURL;
            //beltPictureBox.ImageLocation = character.items[9]._wowHeadImageURL;
            //legsPictureBox.ImageLocation = character.items[10]._wowHeadImageURL;
            //bootsPictureBox.ImageLocation = character.items[11]._wowHeadImageURL;
            //ring1PictureBox.ImageLocation = character.items[12]._wowHeadImageURL;
            //ring2PictureBox.ImageLocation = character.items[13]._wowHeadImageURL;
            //trinket1PictureBox.ImageLocation = character.items[14]._wowHeadImageURL;
            //trinket2PictureBox.ImageLocation = character.items[15]._wowHeadImageURL;
            //mainHandPictureBox.ImageLocation = character.items[16]._wowHeadImageURL;
            //offHandPictureBox.ImageLocation = character.items[17]._wowHeadImageURL;
            //rangedPictureBox.ImageLocation = character.items[18]._wowHeadImageURL;
        }

        private void helmPictureBox_Click(object sender, EventArgs e)
        {
            GenerateTooltip(0);
        }

        private void GenerateTooltip(int slotID)
        {
            if (theCharacter.items[slotID].GetXML())
            {
                itemInfoLabel.Text = theCharacter.items[slotID].GetTooltip();
            }
            else
            {
                MessageBox.Show("Item not found on wowhead", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void neckPictureBox_Click(object sender, EventArgs e)
        {
            GenerateTooltip(1);
        }
    }
}
