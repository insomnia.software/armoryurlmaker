﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;

namespace Armoury_url_maker
{
    public class Character
    {
        public string _name {get; private set;}
        public string _realm {get; private set;}
        public string _title {get; private set;}
        public string _guild {get; private set;}
        public string _items {get; private set;}
        public string _level {get; private set;}
        public string _race {get; private set;}
        public string _spec {get; private set;}
        public string _class {get; private set;}
        public string _achievementPoints {get; private set;}
        public string _ilevel {get; private set;}
        private string XMLDump;
        public List<Item> items {get; private set;}

        public Character(string name, string realm)
        {
            items = new List<Item>();
            _name = char.ToUpper(name[0]) + name.Substring(1);
            _realm = char.ToUpper(realm[0]) + realm.Substring(1);
            _items = ""; // so i can += later
        }

        public bool GetHTML(string url)
        {
            //modified to work offline
            using (WebClient client = new WebClient())
            {
                try
                {
                    XMLDump = client.DownloadString(url);
                    return true;
                }
                catch (WebException) //catch 404
                {
                    return false;
                }
            }           
            //TextReader tr = new StreamReader("armory.htm");
            //XMLDump = tr.ReadToEnd();
            //return true;
            
        }

        public void ParseHTML() //make bool, return false when error caught
        {
            Match match = Regex.Match(XMLDump, "<div class=\"title\">[a-zA-Z ]+</div>");
            _title = match.ToString(); //needs to be parsed further
            if (_title != "")
            {
                _title = _title.Substring(19, _title.Length - 6 - 19); //20 for the first section, 6 for the end section
                _title = _title.Trim();
            }

            match = Regex.Match(XMLDump, "<div class=\"guild\">[ -~\\s]+?</div>");
            _guild = match.ToString();
            match = Regex.Match(_guild, ">[a-zA-Z]+?<");
            _guild = match.ToString();
            _guild = _guild.Substring(1, _guild.Length - 2);

            string temp = Regex.Match(XMLDump, "<span class=\"level\">[ -~\\s]+?</a></div>").ToString(); //errors here

            match = Regex.Match(temp, ">[a-zA-Z]+<");
            _race = match.ToString();
            _race = _race.Substring(1, _race.Length - 2);
            match = match.NextMatch();
            _spec = match.ToString();
            _spec = _spec.Substring(1, _spec.Length - 2);
            match = match.NextMatch();
            _class = match.ToString();
            _class = _class.Substring(1, _class.Length - 2);

            //_achievementPoints = Regex.Match(temp, ">[0-9]+<").ToString();
            MatchCollection  matches = Regex.Matches(temp, ">[0-9]+<");
            _level = matches[0].ToString();
            _level = _level.Substring(1, _level.Length - 2);
            _achievementPoints = matches[1].ToString();
            _achievementPoints = _achievementPoints.Substring(1, _achievementPoints.Length - 2);
            
            match = Regex.Match(XMLDump, "<div data-id=[ -~\\s]+?</div>");
            do
            { 
                Item _item = new Item(match.ToString());
                _item.ParseItem();
                items.Add(_item);

                match = match.NextMatch();
            }
            while (match.ToString() != "");

            foreach (Item iItem in items)
            {
                _items += iItem.ToString();
            }

            _ilevel = Regex.Match(XMLDump, "<span class=\"equipped\">[0-9]+</span>").ToString();
            _ilevel = _ilevel.Substring(23, _ilevel.Length - 23 - 7);

            //int start = 0;
            //int end = 0;
            //StringReader stream = new StringReader(XMLDump);
            //XMLDump = XMLDump.Substring(XMLDump.IndexOf("<div class=\"name\"><a") + 20); //20 is the length of the search string to eliminate that as well
            //string test = XMLDump.Substring(XMLDump.IndexOf("<div class=\"title\">" + 19));
            //start = XMLDump.IndexOf("<div class=\"title\">" + 19);
            //end = XMLDump.IndexOf("</div>");
            //title = XMLDump.Substring(start, end - start);
            #region xmlcode
            //XmlTextReader reader = new XmlTextReader(stream);

            //TextWriter tw = new StreamWriter("output.txt");

            //while (reader.Read())
            //{
            //    //tw.WriteLine("Name: " + XMLReader.Name);
            //    //tw.WriteLine("Node Type: " + XMLReader.NodeType);
            //    //int AttributeCount = XMLReader.AttributeCount;
            //    //tw.WriteLine("Value: " + XMLReader.Value);
            //    switch (reader.NodeType)
            //    {
            //        case XmlNodeType.Element: // The node is an Element.
            //            tw.Write("<" + reader.Name);

            //            while (reader.MoveToNextAttribute()) // Read attributes.
            //                tw.Write(" " + reader.Name + "='" + reader.Value + "'");
            //            tw.Write(">");
            //            tw.WriteLine(">");
            //            break;
            //        case XmlNodeType.Text: //Display the text in each element.
            //            tw.WriteLine(reader.Value);
            //            break;
            //        case XmlNodeType.EndElement: //Display end of element.
            //            tw.Write("</" + reader.Name);
            //            tw.WriteLine(">");
            //            break;
            //    }
            //}
            //tw.Close();
            #endregion
        }

        public override string ToString()
        {
            return "Name: " + _name + "\n" +
                "Title: " + _title + "\n" +
                "Level: " + _level + "\n" +
                "Race: " + _race + "\n" +
                "Class: " + _class + "\n" +
                "Spec: " + _spec + "\n" +
                "Achievement Points: " + _achievementPoints + "\n" +
                "Guild: " + _guild + "\n" +
                "Realm: " + _realm + "\n" +
                "Average ilevel: " + _ilevel + "\n" +
                "Items: " + _items + "\n";
        }
    }
}
