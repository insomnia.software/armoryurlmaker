﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Net;
using System.IO;

namespace Armoury_url_maker
{
    public class Item
    {
        public string _itemName { get; private set; }
        public int _itemSlot { get; private set; }
        public int _itemNumber { get; private set; }
        public int _enchantNumber { get; private set; }
        public int _gem0Number { get; private set; }
        public int _gem1Number { get; private set; }
        public int _gem2Number { get; private set; }
        public int _reforgeNumber { get; private set; }
        public string _imageURL { get; private set; }
        public string _wowHeadImageURL { get; private set; }
        public Image _itemImage { get; private set; }
        private string _itemURL;
        private string _dump;
        private string _XML;

        public Item(string dump)
        {
            _dump = dump;
        }

        public void ParseItem()
        {
            string temp = Regex.Match(_dump, "\"[0-9]+\"").ToString();
            if (temp.Length == 3)
            {
                temp = temp.Substring(1, 1);
            }
            else
            {
                temp = temp.Substring(1, 2);
            }
            _itemSlot = int.Parse(temp);

            temp = Regex.Match(_dump, "<a href=\"/wow/en/item/[0-9]+").ToString();
            temp = Regex.Match(temp, "[0-9]+").ToString();
            if (temp != "")
            {
                _itemNumber = int.Parse(temp);
            }

            temp = Regex.Match(_dump, ";e=[0-9]+").ToString();
            if (temp != "")
            {
                temp = temp.Substring(3);
                _enchantNumber = int.Parse(temp);
            }

            temp = Regex.Match(_dump, "g0=[0-9]+").ToString();
            if (temp != "")
            {
                temp = temp.Substring(3);
                _gem0Number = int.Parse(temp);
            }

            temp = Regex.Match(_dump, "g1=[0-9]+").ToString();
            if (temp != "")
            {
                temp = temp.Substring(3);
                _gem1Number = int.Parse(temp);
            }

            temp = Regex.Match(_dump, "g2=[0-9]+").ToString();
            if (temp != "")
            {
                temp = temp.Substring(3);
                _gem2Number = int.Parse(temp);
            }

            temp = Regex.Match(_dump, "re=[0-9]+").ToString();
            if (temp != "")
            {
                temp = temp.Substring(3);
                _reforgeNumber = int.Parse(temp);
            }

            _imageURL = Regex.Match(_dump, "http://[ -~]+jpg").ToString();

            if (_imageURL != "")
            {
                _wowHeadImageURL = "http://static.wowhead.com/images/wow/icons/large" + Regex.Match(_imageURL, "/[a-z0-9_.]+jpg").ToString();

                using (WebClient client = new WebClient())
                {
                    byte[] imageData = client.DownloadData(_wowHeadImageURL);
                    MemoryStream stream = new MemoryStream(imageData);
                    _itemImage = Image.FromStream(stream);
                    stream.Close();
                }
            }
        }

        public bool GetXML()
        {
            _itemURL = "http://wowhead.com/item=" + _itemNumber + "&xml";
            using (WebClient client = new WebClient())
            {
                try
                {
                    _XML = client.DownloadString(_itemURL);
                    return true;
                }
                catch (WebException) //catch 404 //change to armory at this point
                {
                    return false;
                }
            }
            //TextReader tr = new StreamReader(_itemNumber + ".xml");
            //_XML = tr.ReadToEnd();
            //return true;
        }

        public string GetTooltip()
        {
            _itemName = Regex.Match(_XML, "\\[[A-Za-z ',]+\\]").ToString();
            _itemName = _itemName.Substring(1, _itemName.Length - 2);
            string _binds = Regex.Match(_XML, "<!--bo-->[A-Za-z ]+<").ToString();
            _binds = _binds.Substring(9, _binds.Length - 10);
            string _slot = Regex.Match(_XML, "<tr><td>[A-Za-z]+</td><th>[A-Za-z]+</th>").ToString();
            string _armourType = Regex.Match(_slot, "<th>[A-Za-z]+</th>").ToString();
            if (_armourType != "")
            {
                _armourType = _armourType.Substring(4, _armourType.Length - 4 - 5);
            }
            _slot = Regex.Match(_slot, "<td>[A-Za-z]+</td>").ToString();
            _slot = _slot.Substring(4,_slot.Length - 4 - 5); //error here
            return _itemName + "\n" + _binds + "\n" + _slot + "         " + _armourType;
        }

        public override string ToString()
        {
            return ("Item slot: " + _itemSlot.ToString() + "\n" +
                "Item number: " + _itemNumber.ToString() + "\n" +
                "Enchant: " + _enchantNumber.ToString() + "\n" +
                "Gem 1: " + _gem0Number.ToString() + "\n" +
                "Gem 2: " + _gem1Number.ToString() + "\n" +
                "Gem 3: " + _gem2Number.ToString() + "\n" +
                "Reforge: " + _reforgeNumber.ToString() + "\n");
        }

    }
}
