Windows forms application created to help generate WoW armory links.

It helps with entering foreign characters that english users don't ahve on the keyboard but other players might have used in character names and results in one page load rather than several to load the armory and then search and select results. The armory was very slow at the time and this was valuable.

It also converts old armory links to new ones when Blizzard checged the scheme and scraps the page to return basic info about the character.